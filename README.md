# gitlab-issue-390252

Workaround for issue 390252 https://gitlab.com/gitlab-org/gitlab/-/issues/390252.

Using a !reference tags seems to allows for non-expansion of file type variables.
